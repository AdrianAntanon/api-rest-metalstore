<?php
// Definimos los recursos disponibles
$allowedResourceTypes = [
    'products',
    'authors',
    'genres',
];

// Validamos que el recurso este disponible
$resourceType = $_GET['resource_type'];

if ( !in_array($resourceType, $allowedResourceType)) {
    die;
}

// Defino los recursos
$products = [
    1 => [
        'id' => '1',
        'title' => 'Baphomet',
        'price' => 50.00,
        'description' => 'Baphomet es una deidad asociada al cristianismo de la época medieval, mide 26 cm.',
        'image' => 'assets/images/baphomet.jpg',
    ],
    2 => [
        'id' => '2',
        'title' => 'Botas Demonia',
        'price' => 120.00,
        'description' => 'Increíbles botas góticas, se cuenta que Blade mató a muchos chupasangre con ellas.',
        'image' => 'assets/images/boots-demonia.jpg',
    ],
    3 => [
        'id' => '3',
        'title' => 'Botas Dr.Martens',
        'price' => 180.00,
        'description' => 'Calzado atemporal, si quieres ir cómodo y a la vez agresivo estas son tus botas!',
        'image' => 'assets/images/boots-dr-martens.jpg',
    ],
    4 => [
        'id' => '4',
        'title' => 'Metal Vest Star Wars',
        'price' => 40.00,
        'description' => 'Chaleco de Star Wars, perfecto para cualquier Jedi que se precie.',
        'image' => 'assets/images/chaleco-star-wars.jpg',
    ],
    5 => [
        'id' => '5',
        'title' => 'Chaqueta Iron Maiden',
        'price' => 200.00,
        'description' => 'Chaqueta de cuero con temática de Iron Maiden.',
        'image' => 'assets/images/iron-maiden-jacket.jpg',
    ],
    6 => [
        'id' => '6',
        'title' => 'Pantalones Iron Maiden',
        'price' => 100.00,
        'description' => 'Pantalones tejanos oscuros temáticos de Iron Maiden.',
        'image' => 'assets/images/jeans-iron-maiden.jpg',
    ],
    7 => [
        'id' => '7',
        'title' => 'Camiseta manga larga Kreator',
        'price' => 45.00,
        'description' => 'Kreator - Pleasure to Kill manga larga, imprescindible para ser el más true.',
        'image' => 'assets/images/kreator-t-shirt.jpg',
    ],
    8 => [
        'id' => '8',
        'title' => 'Chaqueta Metallica',
        'price' => 200.00,
        'description' => 'Chaqueta de cuero del mejor disco de Metallica, Ride the lightning.',
        'image' => 'assets/images/metallica-jacket.jpg',
    ],
    9 => [
        'id' => '9',
        'title' => 'Sudadera Rammstein',
        'price' => 60.00,
        'description' => 'Sudadera del mejor grupo alemán de la historia, Rammstein.',
        'image' => 'assets/images/sudadera-rammstein.jpg',
    ],
    10 => [
        'id' => '10',
        'title' => 'Pantalones cortos Arch Enemy',
        'price' => 30.00,
        'description' => 'Pantalones de tela cortos, temática de Arch Enemy.',
        'image' => 'assets/images/shorts-arch-enemy.jpg',
    ],
    11 => [
        'id' => '11',
        'title' => 'Pantalones cortos de Rick and Morty',
        'price' => 30.00,
        'description' => 'Pantalones de tela cortos, inspirados en Pickle Rick de Rick and Morty.',
        'image' => 'assets/images/shorts-pickle-rick.jpg',
    ],
    12 => [
        'id' => '12',
        'title' => 'Pulsera negra',
        'price' => 10.00,
        'description' => 'Pulsera negra estilo hawaiano ',
        'image' => 'assets/images/pulsera.jpg',
    ],
];

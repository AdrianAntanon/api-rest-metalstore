<?php
// Definimos los recursos disponibles
$allowedResourceTypes = [
    'products',
    'bands',
    'genres',
];

// Validamos que el recurso este disponible
$resourceType = $_GET['resource_type'];

if (!in_array($resourceType, $allowedResourceTypes)) {
    http_response_code(400);
    die;
}

// Defino los recursos
$products = [
    [
        'id' => '1',
        'title' => 'Escultura Baphomet',
        'price' => 50.00,
        'description' => 'Baphomet es una deidad asociada al cristianismo de la época medieval, mide 26 cm.',
        'image' => 'assets/images/baphomet.jpg',
    ],
    [
        'id' => '1',
        'title' => 'Escultura Baphomet',
        'price' => 50.00,
        'description' => 'Baphomet es una deidad asociada al cristianismo de la época medieval, mide 26 cm.',
        'image' => 'assets/images/baphomet.jpg',
    ],
    [
        'id' => '2',
        'title' => 'Botas Demonia',
        'price' => 120.00,
        'description' => 'Increíbles botas góticas, se cuenta que Blade mató a muchos chupasangre con ellas.',
        'image' => 'assets/images/boots-demonia.jpg',
    ],
    [
        'id' => '3',
        'title' => 'Botas Dr.Martens',
        'price' => 180.00,
        'description' => 'Calzado atemporal, si quieres ir cómodo y a la vez agresivo estas son tus botas!',
        'image' => 'assets/images/boots-dr-martens.jpg',
    ],
    [
        'id' => '4',
        'title' => 'Metal Vest Star Wars',
        'price' => 40.00,
        'description' => 'Chaleco de Star Wars, perfecto para cualquier Jedi que se precie.',
        'image' => 'assets/images/chaleco-star-wars.jpg',
    ],
    [
        'id' => '5',
        'title' => 'Chaqueta Iron Maiden',
        'price' => 200.00,
        'description' => 'Chaqueta de cuero con temática de Iron Maiden.',
        'image' => 'assets/images/iron-maiden-jacket.jpg',
    ],
    [
        'id' => '6',
        'title' => 'Pantalones Iron Maiden',
        'price' => 100.00,
        'description' => 'Pantalones tejanos cortos temáticos de Iron Maiden.',
        'image' => 'assets/images/jeans-iron-maiden.jpg',
    ],
    [
        'id' => '7',
        'title' => 'Camiseta manga larga Kreator',
        'price' => 45.00,
        'description' => 'Kreator - Pleasure to Kill manga larga, imprescindible para ser el más true.',
        'image' => 'assets/images/kreator-t-shirt.jpg',
    ],
    [
        'id' => '8',
        'title' => 'Chaqueta Metallica',
        'price' => 200.00,
        'description' => 'Chaqueta de cuero del mejor disco de Metallica, Ride the lightning.',
        'image' => 'assets/images/metallica-jacket.jpg',
    ],
    [
        'id' => '9',
        'title' => 'Sudadera Rammstein',
        'price' => 60.00,
        'description' => 'Sudadera del mejor grupo alemán de la historia, Rammstein.',
        'image' => 'assets/images/sudadera-rammstein.jpg',
    ],
    [
        'id' => '10',
        'title' => 'Pantalones Arch Enemy',
        'price' => 30.00,
        'description' => 'Pantalones de tela cortos, temática de Arch Enemy.',
        'image' => 'assets/images/shorts-arch-enemy.jpg',
    ],
    [
        'id' => '11',
        'title' => 'Pantalones Rick and Morty',
        'price' => 30.00,
        'description' => 'Pantalones cortos, inspirados en Pickle Rick de Rick and Morty.',
        'image' => 'assets/images/shorts-pickle-rick.jpg',
    ],
    [
        'id' => '12',
        'title' => 'Pulsera negra',
        'price' => 10.00,
        'description' => 'Pulsera negra estilo hawaiano ',
        'image' => 'assets/images/pulsera.jpg',
    ]
];

// Se indica al cliente que lo que recibirá es un json
header('Content-Type: application/json; charset: UTF-8');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

// Levantamos el id del recurso buscado
$resourceId = array_key_exists('resource_id', $_GET) ? $_GET['resource_id'] : '';

// Generamos la respuesta asumiendo que el pedido es correcto
switch (strtoupper($_SERVER['REQUEST_METHOD'])) {
    case 'GET':
        if (empty($resourceId)) {
            echo json_encode($products);
        } else {
            if (array_key_exists($resourceId, $products)) {
                echo json_encode($products[$resourceId]);
            } else {
                http_response_code(404);
            }
        }
        break;
    case 'POST':
        $json = file_get_contents('php://input');
        $products[] = json_decode($json, true);
        // echo array_keys($products)[count($products) -1];
        echo json_encode($products);
        break;
    case 'PUT':
        // Validamos que el recurso exista
        if (!empty($resourceId) && array_key_exists($resourceId, $products)) {
            $json = file_get_contents('php://input');
            $products[$resourceId] = json_decode($json, true);
            echo json_encode($products);
        }
        break;
    case 'DELETE':
        // Comprobamos si existe
        if (!empty($resourceId) && array_key_exists($resourceId, $products)) {
            // Se elimina el recurso
            unset($products[$resourceId]);
        }
        echo json_encode($products);
        break;
}


// Inicio el servidor en la terminal 1
// php -S localhost:8000 server.php

// Terminal 2 ejecutar 
// curl http://localhost:8000 -v
// curl http://localhost:8000/\?resource_type\=books
// curl http://localhost:8000/\?resource_type\=books | jq